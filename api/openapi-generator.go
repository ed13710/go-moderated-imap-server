// The following directive is necessary to make the package coherent:


// This program generates contributors.go. It can be invoked by running
//go:generate oapi-codegen -generate types  -package api -o  ../pkg/api.go openapi3.yaml
//go:generate oapi-codegen -generate chi-server,spec -package api -o ../pkg/server.go openapi3.yaml

package api
