module gitlab.com/ed13710/go-moderated-imap-server

go 1.13

replace (
	gitlab.com/ed13710/go-moderated-imap => ../go-moderated-imap
	gitlab.com/ed13710/go-moderated-imap-proxy => ../go-moderated-imap-proxy
)

require (
	github.com/deepmap/oapi-codegen v1.3.6
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/emersion/go-imap v1.0.4
	github.com/getkin/kin-openapi v0.3.0
	github.com/go-chi/chi v4.0.4+incompatible
	github.com/go-chi/cors v1.0.1
	github.com/go-chi/jwtauth v4.0.4+incompatible
	github.com/go-chi/render v1.0.1
	github.com/jinzhu/gorm v1.9.12
	github.com/spf13/viper v1.6.2
	gitlab.com/ed13710/go-moderated-imap v0.0.0-20200329165103-f1d245c1505c
	gitlab.com/ed13710/go-moderated-imap-proxy v0.0.0-20200325194701-79134aa3ae3c
	gitlab.com/golocazon/goadmin v0.0.0-20200227215425-b57bf7dceba4
	gitlab.com/golocazon/gologger v0.0.3
)
