package models

type Security int

const (
	SecurityNone Security = iota
	SecuritySTARTTLS
	SecurityTLS
)

type ProxyMailbox struct {
	ID               uint  `json:"id"`
	Username               string  `json:"username"`
	Address                string `json:"address"`
	Security               Security `json:"security"`
	Login                  string `json:"login"`
	Password               string `json:"password "`
	TLSConfig              *ProxyMailboxTLSConfig `json:"tlsconfig"`
	RemoteInboxMailboxName string `json:"remoteinboxname"`
}

type ProxyMailboxTLSConfig struct {
}