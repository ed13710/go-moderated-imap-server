package pkg

import (
	"errors"
	"github.com/jinzhu/gorm"
	dbmodel "gitlab.com/ed13710/go-moderated-imap/model"
)

func NewDBMailboxRepository(db *gorm.DB) *DBMailboxRepository {
	return &DBMailboxRepository{
		DB: db,
	}
}

type DBMailboxRepository struct {
	DB *gorm.DB
}

func (r *DBMailboxRepository) Fetch(offset int64, num uint) (Mailboxs []*dbmodel.ProxyMailbox, err error) {
	//Mailboxs := make([]dbmodel.ProxyMailbox, 0)
	err = r.DB.Limit(num).Offset(offset).Find(&Mailboxs).Error
	return
}

func (r *DBMailboxRepository) GetByID(mailboxID  uint) (error, *dbmodel.ProxyMailbox) {
	Mailbox := &dbmodel.ProxyMailbox{}
	error := r.DB.First(&Mailbox, mailboxID).Error
	if error != nil {
		return errors.New("not found"), Mailbox
	}
	return nil, Mailbox
}

func (r *DBMailboxRepository) Update(Mailbox *dbmodel.ProxyMailbox) (*dbmodel.ProxyMailbox, error) {
	// Create a new Mailbox
	error := r.DB.Save(&Mailbox).Error
	return Mailbox, error
}

func (r *DBMailboxRepository) Store(Mailbox *dbmodel.ProxyMailbox) (*dbmodel.ProxyMailbox, error) {
	// Create a new Mailbox
	error := r.DB.Create(&Mailbox).Error
	//r.DB.Save(&Mailbox)
	return Mailbox, error
}

func (r *DBMailboxRepository) Delete(MailboxID uint) (bool, error) {
	Mailbox := &dbmodel.ProxyMailbox{}
	Mailbox.ID = MailboxID
	error := r.DB.Delete(&Mailbox).Error
	if error != nil {
		return false, errors.New("not found")
	}
	return true, nil
}
