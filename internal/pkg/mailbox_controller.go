package pkg

import (
	"gitlab.com/ed13710/go-moderated-imap-server/internal/pkg/models"
)

type MailboxController interface {
	CreateMailbox(mailbox models.ProxyMailbox) (error, *models.ProxyMailbox)

	UpdateMailbox(MailboxID uint, mailbox models.ProxyMailbox) (error, *models.ProxyMailbox)

	GetMailboxes() (error, []*models.ProxyMailbox)

	GetMailbox(MailboxID uint) (error, *models.ProxyMailbox)

	DeleteMailbox(MailboxID uint) error

}
