package pkg

import (
	"github.com/jinzhu/gorm"
	model "gitlab.com/ed13710/go-moderated-imap-server/internal/pkg/models"
	dbmodel "gitlab.com/ed13710/go-moderated-imap/model"
)



func NewApiMailbox(mailbox *dbmodel.ProxyMailbox) *model.ProxyMailbox {
	return &model.ProxyMailbox{
		ID:						mailbox.ID,
		Username:               mailbox.Username,
		Address:                mailbox.Address,
		Security:               0,
		Login:                  mailbox.Login,
		Password:               mailbox.Password,
		TLSConfig:              nil,
		RemoteInboxMailboxName: mailbox.RemoteInboxMailboxName,
	}
}


func NewDBMailbox(mailbox *model.ProxyMailbox) *dbmodel.ProxyMailbox {
	return &dbmodel.ProxyMailbox{
		Model: gorm.Model {
			ID:               		mailbox.ID,

		},
		Username:               mailbox.Username,
		Address:                mailbox.Address,
		Security:               0,
		Login:                  mailbox.Login,
		Password:               mailbox.Password,
		TLSConfig:              nil,
		RemoteInboxMailboxName: mailbox.RemoteInboxMailboxName,
	}
}
