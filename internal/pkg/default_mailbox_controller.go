package pkg

import (
	"errors"
	model "gitlab.com/ed13710/go-moderated-imap-server/internal/pkg/models"
)

func NewDefaultMailboxController(mailboxRepository  MailboxRepository) *DefaultMailboxController {
	return &DefaultMailboxController{
		MailboxRepository: mailboxRepository,
	}
}

type DefaultMailboxController struct {
	MailboxRepository MailboxRepository
}


func (r *DefaultMailboxController) CreateMailbox(mailbox model.ProxyMailbox) (error, *model.ProxyMailbox) {
	// Create a new User
	dbMailbox  := NewDBMailbox(&mailbox)
	createdMailbox, error := r.MailboxRepository.Store(dbMailbox)
	return error, NewApiMailbox(createdMailbox)
}


func (r *DefaultMailboxController) UpdateMailbox(mailboxID uint, mailbox model.ProxyMailbox) (error, *model.ProxyMailbox) {
	err, loadedMailbox := r.MailboxRepository.GetByID(mailboxID)
	if err != nil {
		return errors.New("Mailbox does not exist"), nil
	}
	mailbox.ID=loadedMailbox.ID
	updatedMailbox := NewDBMailbox(&mailbox)

	// Create a new User
	updatedMailbox, error := r.MailboxRepository.Update(updatedMailbox)
	return error, NewApiMailbox(updatedMailbox)
}


func (r *DefaultMailboxController) GetMailbox(mailboxID uint) (err error, loadedMailbox *model.ProxyMailbox) {
	err, dbMailbox :=  r.MailboxRepository.GetByID(mailboxID)
	loadedMailbox = NewApiMailbox(dbMailbox)
	return
}

func (r *DefaultMailboxController) GetMailboxes() (err error, loadedMailboxes []*model.ProxyMailbox) {
	dbMailboxes,err :=  r.MailboxRepository.Fetch(0, 10)
	loadedMailboxes = make ([]*model.ProxyMailbox, len(dbMailboxes))
	for i, _ := range(dbMailboxes) {
		loadedMailboxes[i] = NewApiMailbox(dbMailboxes[i])
	}
	return
}

func (r *DefaultMailboxController) DeleteMailbox(mailboxID uint) (err error) {
	_, err = r.MailboxRepository.Delete(mailboxID)
	return
}
