package pkg

import (
	"gitlab.com/ed13710/go-moderated-imap/model"
)

type MailboxRepository interface {
	Fetch(offset int64, num uint) ([]*model.ProxyMailbox, error)
	GetByID(mailboxID uint) (error, *model.ProxyMailbox,)
	Update(Mailbox *model.ProxyMailbox,) (*model.ProxyMailbox, error)
	Store(a *model.ProxyMailbox,) (*model.ProxyMailbox, error)
	Delete(id uint) (bool, error)
}

