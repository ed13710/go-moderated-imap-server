package internal

import (
	"github.com/jinzhu/gorm"
	go_moderated_imap "gitlab.com/ed13710/go-moderated-imap"
	logger "gitlab.com/golocazon/gologger"
)

func ConfigDatabaseUrl(dialect, dbUrl string, logging bool) *gorm.DB{

	db, err := gorm.Open(dialect, dbUrl)
	if err != nil {
		//logger := logger.WithFields(logger.Fields{"key1": "value1"})
		logger.Panicf("failed to connect database", err)

	}
	db.LogMode(logging)
	// Migrate the schema
	go_moderated_imap.InitDatabase(db)
	return db
}

