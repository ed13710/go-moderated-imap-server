package internal

import (
	"encoding/json"
	"github.com/go-chi/chi"
	"github.com/go-chi/jwtauth"
	"gitlab.com/ed13710/go-moderated-imap-server/internal/pkg"
	models "gitlab.com/ed13710/go-moderated-imap-server/internal/pkg/models"
	"gitlab.com/golocazon/gologger"
	"net/http"
	"strconv"
)

type  DefaultServerInterface struct{
	MailboxController pkg.MailboxController
}

func NewMailboxServerInterface(pc pkg.MailboxController) *DefaultServerInterface {
	return &DefaultServerInterface {
		MailboxController : pc,
	}
}
	// patch a mailbox (DELETE /mailbox/{mailboxId})
func (si * DefaultServerInterface)		DeleteMailbox(w http.ResponseWriter, r *http.Request) {
	//mailboxID, _ := getMailboxId(r)

	mailboxIDAsString, ok := r.Context().Value("mailboxId").(string)
	if (!ok) {
		http.Error(w, http.StatusText(400), 400)
		return
	}
	mailboxID, err := strconv.ParseUint(mailboxIDAsString, 0, 0)
	if (err != nil) {
		http.Error(w, http.StatusText(400), 400)
		return
	}
	err = si.MailboxController.DeleteMailbox(uint(mailboxID))
	if err != nil {
		http.Error(w, http.StatusText(404), 404)
		return
	}
	//render.Status(r, http.StatusOK )
	//render.Render(w, r, &ProductsResponse{ Products: &Products,})

}
	// get a mailbox (GET /mailbox/{mailboxId})
func (si * DefaultServerInterface)		GetMailbox(w http.ResponseWriter, r *http.Request) {
	//mailboxID, _ := getMailboxId(r)
	requester, _ := getRequester(r)
	gologger.Log.Infof("Requester is %v",requester)
	mailboxIDAsString, ok := r.Context().Value("mailboxId").(string)
	if (!ok) {
		http.Error(w, http.StatusText(400), 400)
		return
	}
	mailboxID, err := strconv.ParseUint(mailboxIDAsString, 0, 0)
	if (err != nil) {
		http.Error(w, http.StatusText(400), 400)
		return
	}
	err, mailbox := si.MailboxController.GetMailbox(uint(mailboxID))
	if err != nil {
		http.Error(w, http.StatusText(404), 404)
		return
	}
	//render.Status(r, http.StatusOK )
	//render.Render(w, r, &ProductsResponse{ Products: &Products,})
	js, err := json.Marshal(mailbox)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.Write(js)
}
	// patch a mailbox (PUT /mailbox/{mailboxId})
func (si * DefaultServerInterface)		UpdateMailbox(w http.ResponseWriter, r *http.Request) {
	//mailboxID, _ := getMailboxId(r)

	mailboxIDAsString, ok := r.Context().Value("mailboxId").(string)
	if (!ok) {
		http.Error(w, http.StatusText(400), 400)
		return
	}
	mailboxID, err := strconv.ParseUint(mailboxIDAsString, 0, 0)
	if (err != nil) {
		http.Error(w, http.StatusText(400), 400)
		return
	}
	var mailbox models.ProxyMailbox
	err = json.NewDecoder(r.Body).Decode(&mailbox)
	if err != nil {
		http.Error(w, http.StatusText(500), 500)
		return
	}
	err, updatedMailbox := si.MailboxController.UpdateMailbox(uint(mailboxID), mailbox)
	if err != nil {
		http.Error(w, http.StatusText(404), 404)
		return
	}
	//render.Status(r, http.StatusOK )
	//render.Render(w, r, &ProductsResponse{ Products: &Products,})
	js, err := json.Marshal(updatedMailbox)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.Write(js)
}
	// searches mailboxs (GET /mailboxs)
func (si * DefaultServerInterface)	GetMailboxes(w http.ResponseWriter, r *http.Request) {
	//mailboxID, _ := getMailboxId(r)

	err, mailboxs := si.MailboxController.GetMailboxes()
	if err != nil {
		http.Error(w, http.StatusText(404), 404)
		return
	}
	//render.Status(r, http.StatusOK )
	//render.Render(w, r, &ProductsResponse{ Products: &Products,})
	js, err := json.Marshal(mailboxs)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.Write(js)
}
	// adds a mailbox (POST /mailboxs)
func (si * DefaultServerInterface)		AddMailbox(w http.ResponseWriter, r *http.Request) {
	//mailboxID, _ := getMailboxId(r)
	//requester, _ := getRequester(r)
	//gologger.Log.Infof("Requester is %v",requester)

	var mailbox models.ProxyMailbox
	err := json.NewDecoder(r.Body).Decode(&mailbox)
	if err != nil {
		http.Error(w, http.StatusText(500), 500)
		return
	}

	err, createdMailbox := si.MailboxController.CreateMailbox(mailbox)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	js, err := json.Marshal(createdMailbox)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.Write(js)
}


func getRequester(r *http.Request) (mailboxID int64, err error) {
	_, claims, _ := jwtauth.FromContext(r.Context())
	mailboxIDAsFloat64 := claims["mailbox_id"].(float64)
	mailboxID = int64(mailboxIDAsFloat64)
	return
}


func getMailboxId(r *http.Request) (mailboxID int64, err error) {
	mailboxIDVal := chi.URLParam(r, "mailboxId")
	if len(mailboxIDVal) > 0 {
		mailboxIDAsInt, _ := strconv.Atoi(mailboxIDVal)
		mailboxID = int64(mailboxIDAsInt)
	}
	return
}
