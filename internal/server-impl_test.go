package internal

import (
	"bytes"
	"github.com/dgrijalva/jwt-go"
	"github.com/go-chi/jwtauth"
	"gitlab.com/ed13710/go-moderated-imap-server/internal/pkg"

	"io"
	"net/http"
	"net/http/httptest"
	"testing"

	 _ "github.com/jinzhu/gorm/dialects/sqlite"

	)

func initAdminServerInterface() *DefaultServerInterface {
	database := ConfigDatabaseUrl("sqlite3", ":memory:", true)
	mailboxRepository := pkg.NewDBMailboxRepository(database)
	mailboxController := pkg.NewDefaultMailboxController(mailboxRepository)

	return  NewMailboxServerInterface(mailboxController)
}

func TestDefaultServerInterface_AddUser(t *testing.T) {


	dsi := initAdminServerInterface()

	expected := `{"id":1,"name":"test"}`
	testRequest(dsi.AddMailbox, "/users", "POST", bytes.NewReader([]byte(`{"name":"test"}`)), &expected, http.StatusOK, t)
	expected2 := `[{"id":1,"name":"test"}]`
	testRequest(dsi.AddMailbox, "/users", "GET", nil, &expected2, http.StatusOK, t)


}

func testRequest( handler http.HandlerFunc, path string, method string, bodyInput io.Reader, expected *string,  expectedCode int, t *testing.T) {
	req, err := http.NewRequest(method, path, bodyInput)
	if err != nil {
		t.Fatal(err)
	}
	rr := httptest.NewRecorder()
	// e.g. func GetUsersHandler(ctx context.Context, w http.ResponseWriter, r *http.Request)


	// Populate the request's context with our test data.
	ctx := req.Context()
	//ctx = context.WithValue(ctx, "app.auth.token", "abc123")
	//ctx = context.WithValue(ctx, "app.user",
	//	&YourUser{ID: "qejqjq", Email: "user@example.com"})

	// Add our context to the request: note that WithContext returns a copy of
	// the request, which we must assign.
	req = req.WithContext(ctx)
	handler.ServeHTTP(rr, req)

	// Check the status code is what we expect.
	if status := rr.Code; status != expectedCode {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, expectedCode)
	}
	// Check the response body is what we expect.
	if expected != nil && rr.Body.String() != *expected {
		t.Errorf("handler returned unexpected body: got %v want %v",
			rr.Body.String(), *expected)
	}
}


func TestDefaultServerInterface_AddUser2(t *testing.T) {
	req, err := http.NewRequest("GET", "/api/users", bytes.NewReader([]byte("{ \"name\": \"test\" }")))
	if err != nil {
		t.Fatal(err)
	}

	rr := httptest.NewRecorder()
	// e.g. func GetUsersHandler(ctx context.Context, w http.ResponseWriter, r *http.Request)
	database := ConfigDatabaseUrl("sqlite3", ":memory:", true)
	mailboxRepository := pkg.NewDBMailboxRepository(database)
	mailboxController := pkg.NewDefaultMailboxController(mailboxRepository)

	dsi :=  NewMailboxServerInterface(mailboxController)
	handler := http.HandlerFunc(dsi.AddMailbox)

	// Populate the request's context with our test data.
	ctx := req.Context()
	tokenAuth := jwtauth.New("HS256", []byte("secret"), nil)
	token, _, _ := tokenAuth.Encode(jwt.MapClaims{"user_id": float64(123)})

	ctx = jwtauth.NewContext(ctx, token, nil)
	//ctx = context.WithValue(ctx, "app.auth.token", "abc123")
	//ctx = context.WithValue(ctx, "app.user",
	//	&YourUser{ID: "qejqjq", Email: "user@example.com"})

	// Add our context to the request: note that WithContext returns a copy of
	// the request, which we must assign.
	req = req.WithContext(ctx)
	handler.ServeHTTP(rr, req)

	// Check the status code is what we expect.
	if status := rr.Code; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}
	// Check the response body is what we expect.
	expected := `{"id":1,"name":"test"}`
	if rr.Body.String() != expected {
		t.Errorf("handler returned unexpected body: got %v want %v",
			rr.Body.String(), expected)
	}
}
