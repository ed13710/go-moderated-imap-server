package main

import (
	"github.com/dgrijalva/jwt-go"
	"github.com/emersion/go-imap/server"
	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
	"github.com/go-chi/cors"
	"github.com/go-chi/jwtauth"
	"github.com/go-chi/render"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
	"github.com/spf13/viper"
	mproxy "gitlab.com/ed13710/go-moderated-imap-proxy"
	"gitlab.com/ed13710/go-moderated-imap-server/internal"
	"gitlab.com/ed13710/go-moderated-imap-server/internal/pkg"
	api "gitlab.com/ed13710/go-moderated-imap-server/pkg"
	logger "gitlab.com/golocazon/gologger"
	"log"
	"net/http"
	"strconv"
)

var (
	database  *gorm.DB
	tokenAuth *jwtauth.JWTAuth
)


func main() {

	configConfiguration()
	configLogging()
	configJwt()
	database = configDatabase()
	defer database.Close()


	// Create a memory backend
	be := mproxy.New(database)

	// Create a new server
	s := server.New(be)
	s.Addr = ":1143"
	// Since we will use this server for testing only, we can allow plain text
	// authentication over unencrypted connections
	s.AllowInsecureAuth = true

	go startServer()
	log.Println("Starting IMAP server at localhost:1143")
	if err := s.ListenAndServe(); err != nil {
		log.Fatal(err)
	}
}





func startServer() {
	// Basic CORS
	// for more ideas, see: https://developer.github.com/v3/#cross-origin-resource-sharing
	cors := cors.New(cors.Options{
		// AllowedOrigins: []string{"https://foo.com"}, // Use this to allow specific origin hosts
		AllowedOrigins: []string{"*"},
		// AllowOriginFunc:  func(r *http.Request, origin string) bool { return true },
		AllowedMethods:   []string{"GET", "POST", "PUT", "DELETE", "OPTIONS"},
		AllowedHeaders:   []string{"Accept", "Authorization", "Content-Type", "X-CSRF-Token"},
		ExposedHeaders:   []string{"Link"},
		AllowCredentials: true,
		MaxAge:           300, // Maximum value not ignored by any of major browsers
	})

	mux := chi.NewRouter()

	//csrfToken := []byte("32-byte-long-auth-key")
	//csrf := csrf.Protect(csrfToken)
	// The middlewares we're using:
	// - logger just does basic logging of requests and debug info
	// - nosurfing is a more verbose wrapper around csrf handling
	// - LoadClientStateMiddleware is required for session/cookie stuff
	// - remember middleware logs users in if they have a remember token
	// - dataInjector is for putting data into the request context we need for our template layout
	// A good base middleware stack
	mux.Use(cors.Handler)

	mux.Use(middleware.RequestID)
	mux.Use(middleware.Logger)
	mux.Use(middleware.Recoverer)
	mux.Use(middleware.URLFormat)
	// Seek, verify and validate JWT tokens
	//mux.Use(jwtauth.Verifier(tokenAuth))

	// Handle valid / invalid tokens. In this example, we use
	// the provided authenticator middleware, but you can write your
	// own very easily, look at the Authenticator method in jwtauth.go
	// and tweak it, its not scary.
	//mux.Use( jwtauth.Authenticator)

	mux.Use(render.SetContentType(render.ContentTypeJSON))
	mux.Get("/ping", func(w http.ResponseWriter, r *http.Request) {
		w.Write([]byte("pong"))
	})

	mailboxRepository := pkg.NewDBMailboxRepository(database)
	mailboxController := pkg.NewDefaultMailboxController(mailboxRepository)

	dsi := internal.NewMailboxServerInterface(mailboxController)

	subMux := mux //mux.With(jwtauth.Verifier(tokenAuth), jwtauth.Authenticator)
	api.HandlerFromMux(dsi, subMux)
	// Start the server
	port := viper.GetInt("binding.port")
	host := viper.GetString("binding.host")
	if port == 0 {
		port = 1320
	}
	if len(host) == 0 {
		host = "localhost"
	}

	log.Printf("Listening on localhost: %s", port)
	log.Println(http.ListenAndServe(host+":"+strconv.Itoa(port), mux))

}

func configConfiguration() {
	viper.SetConfigName("mproxy_config") // name of config file (without extension)
	viper.AddConfigPath("$HOME/.goadmin") // call multiple times to add many search paths
	viper.AddConfigPath(".")              // optionally look for config in the working directory

	err := viper.ReadInConfig() // Find and read the config file
	if err != nil {             // Handle errors reading the config file
		//		panic(fmt.Errorf("Fatal error config file: %s \n", err))
	}
}

func configDatabase() *gorm.DB {
	dbUser := viper.GetString("db.user")
	dbPass := viper.GetString("db.password")
	dbHost := viper.GetString("db.host")
	dbPort := viper.GetInt("db.port")
	dbSchema := viper.GetString("db.schema")
	logging := viper.GetBool("db.logging")
	dbUrl := dbUser + ":" + dbPass + "@tcp(" + dbHost + ":" + strconv.Itoa(dbPort) + ")/" + dbSchema + "?charset=utf8&parseTime=True&loc=Local"
	log.Printf("Db URL  : %v", dbUrl)

	return internal.ConfigDatabaseUrl("mysql", dbUrl, logging)
}


func configJwt() {
	secretString := viper.GetString("secretString")
	tokenAuth = jwtauth.New("HS256", []byte(secretString), nil)
	_, tokenString, _ := tokenAuth.Encode(jwt.MapClaims{"user_id": 123})
	logger.Infof("DEBUG: a sample jwt is %s\n\n", tokenString)
}

func configLogging() {
	config := logger.Configuration{
		EnableConsole:     true,
		ConsoleLevel:      logger.Debug,
		ConsoleJSONFormat: true,
		EnableFile:        true,
		FileLevel:         logger.Info,
		FileJSONFormat:    true,
		FileLocation:      "log.log",
	}
	err := logger.NewLogger(config, logger.InstanceZapLogger)
	if err != nil {
		log.Fatalf("Could not instantiate log %s", err.Error())
	}

	contextLogger := logger.WithFields(logger.Fields{"key1": "value1"})
	contextLogger.Debugf("Starting with zap")
	contextLogger.Infof("Zap is awesome")

	err = logger.NewLogger(config, logger.InstanceLogrusLogger)
	if err != nil {
		log.Fatalf("Could not instantiate log %s", err.Error())
	}
	contextLogger = logger.WithFields(logger.Fields{"key1": "value1"})
	contextLogger.Debugf("Starting with logrus")

	contextLogger.Infof("Logrus is awesome")
}
